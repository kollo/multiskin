/****************************************************************************
 **									   **
 **				MULTISKIN				   **
 **									   **
 **									   **
 **  Programm zum Berechnen von Wirbelstroemen in einer 2D Leitergeometrie **
 **									   **
 **  frei nach einem Mathematica-Programm  von T. Toyama		   **
 ** 									   **
 **  Erstellt: Aug. 1997   von Markus Hoffmann				   **
 ** 									   **
 **  Letzte Bearbeitung: Mar. 1998					   **
 **									   **
 ****************************************************************************/
/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */

#define VERSION "2.00"
#define VDATE   "11.05.2019"

#define CACHEFILE "mat-cache.bin"



#define PI       3.141592654

#define FALSE    0
#define TRUE     (!FALSE)
#define E        2.718281828

#define MAXSTRLEN 512
#define MAXLINELEN 256

#define round(a) ((int)(a+0.5))
#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a>b)?a:b)
#define sgn(x)   ((x<0)?-1:1)
#define rad(x)   (3.141592654*x/180)
#define deg(x)   (180*x/3.141592654)


#define mm       1e-3
#define sigSUS   1.4e6   /* spezifische Leitfaehigkeiten   */
#define sigCu    5.8e7

#define mu0      (4e-7*PI)

/*Define output data formats*/

#define FORMAT_PLAIN 0
#define FORMAT_TOML  1

extern int output_format;


/* definiert einen Leiter aus der Eingangsgeometrie */
typedef struct {
  double xmin;   /* Geometrie des Rechteckigen Leiters*/
  double xmax;
  double ymin;
  double ymax;
  double laenge; /* Länge des Leiters*/
  double strom;  /* Strom durch Leiter (von aussen) */
  double rho;    /* Leitfähigkeit / material */
  double Vr;     /* Berechnete Induktionsspannung/Spannungsabfall, realteil*/
  double Vi;     /* Berechnete Induktionsspannung/Spannungsabfall, imaginaeteil*/
  double itotr;
  double itoti;
} LEITER;

typedef struct {
  int leiter;     /* zu Filament gehoerender Leiter */
  VEC2D position; /* x,y Pos des Filaments	     */
  double ijr;     /* realteil */
  double iji;     /* Imaginaerteil */
  double ijabs;   /* Absolutbetrag */  
} FILAMENT;

/* Definiert die Physik an einem Mesh-Punkt*/
typedef struct {
  int filament; /* Zugehörige Filamentnummer oder -1*/
  VEC3D Br;    /* Magnetfeld */
  VEC3D Bi;    /* Magnetfeld */
  double rho; /* Stromdichte */
} MESH;

extern MESH *mesh;     /* 2D Raum */
extern FILAMENT *filamente; /* Einzelne Strom-Filamente*/
extern LEITER *leiter; /* Leitergeometrie aus input file */

extern int verbose;
extern int domag;  /* Soll das Magnetfeld ausgegeben werden ? */
extern int donew;  /* Soll das Mesh neu berechnet werden ?  */
extern double r0;
extern double w;  /* Omega = Frequenz         */

extern int  ncoil;       /* Anzahl der Leiter  */


/* Function prototypes */

void multiskin_free();
void read_file(char *name);
void do_calc();
void magnetfeld();

#ifdef WINDOWS
  #define bzero(p, l) memset(p, 0, l)
#endif
