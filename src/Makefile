# Makefile for multiskin (c) Markus Hoffmann V.2.00

# This file is part of MULTISKIN, the Eddi Current Calculator
# ============================================================
# MULTISKIN is free software and comes with NO WARRANTY - 
# read the file COPYING for details.

# Insert the defs for your machine

SHELL=/bin/sh

LIBNO=2.00
RELEASE=2
NAME=multiskin

# Directories
prefix=/usr
exec_prefix=${prefix}
datarootdir=${prefix}/share

BINDIR=${exec_prefix}/bin
LIBDIR=${exec_prefix}/lib
DATADIR=${datarootdir}
MANDIR=${datarootdir}/man
INCDIR=${prefix}/include/multiskin

DIR=$(NAME)-$(LIBNO)


# Register variables (-ffixed-reg)
REGS= -fno-omit-frame-pointer
# Optimization and debugging options
OPT=-O3

# Additional header file paths
INC= -I.

# Compiler
#CC=gcc  $(REGS)
CC=gcc -Wall
CCDEBUG=gcc -g 
CCSHARED=gcc -fPIC -shared -Wl,-Bsymbolic-functions

# Cross-Compiler fuer Windows-Excecutable
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
ARMCC=arm-linux-gcc

# Preprocessor
CPP=gcc -E

CFLAGS= $(INC) $(DEF) $(OPT) $(REGS)

# Name of the excecutable
EXE=	$(NAME)
WINEXE= $(NAME).exe

# Headerfiles which should be added to the distribution
HSRC=	multiskin.h file.h tools.h matrix.h vector.h geometry.h

CSRC=	main.c multiskin.c file.c tools.c matrix.c vector.c geometry.c

MAN=	multiskin.1

DOC=	../RELEASE_NOTES ../README.md ACKNOWLEGEMENTS AUTHORS
DEBDOC= copyright changelog.Debian $(DOC)
WINDOC= WINDOWS/readme-windows.txt WINDOWS/lib/README-LAPACK.txt

DIST= INSTALL ../COPYING ../CONTRIBUTING.md \
      $(HSRC) $(CSRC) $(DOC) $(MAN) Makefile

BINDIST= ../COPYING $(MAN) $(DOC) $(EXE)

WINDIST= $(WINEXE) $(WINDOC) \
	WINDOWS/multiskin.ico  WINDOWS/test.inp \
	../COPYING ../RELEASE_NOTES INTRO

LIBS = -llapack -lm

OBJS= $(CSRC:.c=.o)


all: 	$(EXE)
$(EXE): $(OBJS) 
	gcc -g $(OPT) -Wall -o $@ $(OBJS) $(LIBS)

# Make the exe for MS WINDOWS
$(WINEXE): $(CSRC) $(HSRC) Makefile 
	$(WINCC) $(OPT) -DWINDOWS -Wall -o $@ $(CSRC) \
	WINDOWS/liblapack.lib WINDOWS/libblas.lib -lm
	strip $@

test:	multiskin
	./multiskin --new test.inp  --mesh 0.1 --freq 1  --mag -v -v 
	gnuplot ../gnu/plot.gnu
	rm -f blech 
test2:	multiskin
	./multiskin --new ../geometries/geometry.inp  --mesh 1 --freq 1  --mag > blech  
#	meld . ../data/1997-09-03/f1o

install: $(NAME) $(NAME).1
	install -d $(BINDIR)
	install -s -m 755 $(NAME) $(BINDIR)/
	install -d $(MANDIR)
	install -d $(MANDIR)/man1
	install -m 644 $(NAME).1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/man1/$(NAME).1
	rmdir --ignore-fail-on-non-empty $(BINDIR)
	rmdir --ignore-fail-on-non-empty $(MANDIR)/man1
	rmdir --ignore-fail-on-non-empty $(MANDIR)

doc-pak: $(DEBDOC)
	mkdir -p $@
	cp $+ $@/
	gzip -n -9 $@/changelog.Debian

# Make a tar ball with the sources  

dist :	$(NAME)-$(LIBNO).tar.gz
$(NAME)-$(LIBNO).tar.gz : $(DIST)
	rm -rf /tmp/$(NAME)-$(LIBNO)
	mkdir /tmp/$(NAME)-$(LIBNO)
	(tar cf - $(DIST))|(cd /tmp/$(NAME)-$(LIBNO); tar xpf -)
	(cd /tmp; tar cf - $(NAME)-$(LIBNO)|gzip -9 > $@)
	mv /tmp/$@ .

# Make a simple debian package which can easily be installed and removed 
# from the system

deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) \
	--pkgrelease $(RELEASE)  \
	--maintainer kollo@users.sourceforge.net \
	--requires libc6 --backup \
	--pkggroup science \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	rm -f backup-*.tgz
	sudo chown 1000 $(NAME)_$(LIBNO)-$(RELEASE)_*.deb

windows: $(WINDIST) $(NAME).iss
	rm -f $(NAME)-$(LIBNO)-$(RELEASE)-win.zip
	zip -j -D  -o $(NAME)-$(LIBNO)-$(RELEASE)-win.zip $(WINDIST)
	iscc $(NAME).iss

dataclean :
	rm -f *.dat *.eps invcmat.dta
clean: dataclean
	rm -f *.o a.out Makefile.bak backup-*.tgz *.d
distclean: clean
	rm -f $(EXE) $(WINEXE)
	rm -rf doc-pak


