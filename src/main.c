/* main.c

   (c) Markus Hoffmann 1997-1998 
*/

/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <fcntl.h> 

#ifndef WINDOWS
#include <sysexits.h>
#else
  #define EX_OK 0
  #define EX_UNAVAILABLE -1
  #define EX_NOINPUT -1
  #define EX_IOERR -1
#endif

#include "file.h"
#include "vector.h"
#include "multiskin.h"

char ifilename[256]="input.geo";       /* Geometriefile             */

/* DEFAULT-Flags     */

int donew=FALSE;  /* Soll das Mesh neu berechnet werden ?  */

int domag=FALSE;  /* Soll das Magnetfeld ausgegeben werden ? */
int dorund=FALSE; /* Runde oder eckige Filamente ?  */
int verbose=0;    /* Verbosity level*/
int loadfile=FALSE;

double r0            = 1.0*mm;        /* Meshweite                */
/*double leiterlaenge  = 0.5;            Laenge des Magneten in m */
double w             = 2* PI * 5000;  /* Omega = Frequenz         */

static void intro(){
  printf("*********************************************\n"
         "*  MULTI-SKINEFF          V. " VERSION "           *\n"
         "*    von Markus Hoffmann 1997               *\n"
         "*                                           *\n"
         "* Copyright (c) 1997-1998 -- Physikalisches *\n"
	 "*    Institut der Universitaet Bonn         *\n" 
         "* Programmversion vom " VDATE "            *\n"
         "*********************************************\n\n"); 
}
static void usage(){
  puts("\n Usage:\n ------ \n");
  printf(" %s [-h] [<filename>] --- process geometry file [%s]\n\n","multiskin",ifilename);
  printf("  --new\t\t--- Matrizen neu berechnen\t[%d]\n",donew);
  printf("  --mag\t\t--- Magnetfeld berechnen\t[%d]\n",domag);
  printf("  --freq <x>\t--- Frequenz  in Hz\t[%d]\n",(int)(w/2/PI));
  printf("  --mesh <x>\t--- Meshweite in mm\t[%g]\n",r0/mm);
  printf("  --version\t--- Print version information and license\n"
         "  --toml\t\t---\tuse TOML output format\n"
         "  -h --help\t--- usage\n"
         "  -v\t\t--- be more verbose\n"
         "  -q\t\t--- be more quiet\n"
	 );
}

/* parse the command line parameters */

static void kommandozeile(int anzahl, char *argumente[]) {
  int count,quitflag=0;

  /* Kommandozeile bearbeiten   */

  for(count=1;count<anzahl;count++) {
    if(!strcmp(argumente[count],"--help") || !strcmp(argumente[count],"-h")) {
      intro(); usage(); quitflag=1;
    } else if (strcmp(argumente[count],"--version")==FALSE) {
      intro();
      puts("This program is scientific software. The used methods and formulas are\n" 
            "published in: Markus Hoffmann, 'Bau und Test gepulster Quadrupolmagnete \n" 
            "zur Querung intrinsischer depolarisierender Resonanzen in ELSA', \n"
	    "BONN-IB-98-10, Bonn University, March 1998.\n\n"
	    "This program is open source and licensed under the GPLv2."
	    "This program is distributed in the hope that it will be useful, "
	    "but WITHOUT ANY WARRANTY; without even the implied warranty of "
    	    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.");
    }
    else if(!strcmp(argumente[count],"--freq")) w=2*PI*atof(argumente[++count]); 
    else if(!strcmp(argumente[count],"--mesh")) r0=atof(argumente[++count])*mm;
    else if(!strcmp(argumente[count],"--new")) donew=TRUE;
    else if(!strcmp(argumente[count],"--mag")) domag=TRUE;
    else if(!strcmp(argumente[count],"--toml")) output_format=FORMAT_TOML;
    else if(!strcmp(argumente[count],"-r"))   dorund=TRUE;
    else if(!strcmp(argumente[count],"-v")) verbose++;
    else if(!strcmp(argumente[count],"-q")) verbose--;
    else if(*(argumente[count])=='-') {
      printf("multiskin: Unknown option: %s\n",argumente[count]);
    } else {
      if(!loadfile) {
        loadfile=TRUE;
        strcpy(ifilename,argumente[count]); 
      }
    }
  }
  if(quitflag) exit(EX_OK);
}



int main(int anzahl, char *argumente[]) {
  if(anzahl<2) {
    intro();usage();
    exit(EX_OK);
  } else kommandozeile(anzahl, argumente);    /* Kommandozeile bearbeiten */


  if(!exist(ifilename)) {perror(ifilename);return(EX_NOINPUT);}

  read_file(ifilename);

  do_calc();
  if(domag) magnetfeld();
  
  multiskin_free();  /* free memory */
  return(EX_OK);
}

