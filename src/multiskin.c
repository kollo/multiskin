/****************************************************************************
 **									   **
 **				MULTISKIN				   **
 **									   **
 **									   **
 **  Programm zum Berechnen von Wirbelstroemen in einer 2D Leitergeometrie **
 **									   **
 **  frei nach einem Mathematica-Programm  von T. Toyama		   **
 ** 									   **
 **  Erstellt: Aug. 1997   von Markus Hoffmann				   **
 ** 									   **
 **  Letzte Bearbeitung: May 2019					   **
 **									   **
 ****************************************************************************/

/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "file.h"
#include "matrix.h"
#include "vector.h"
#include "geometry.h"

#include "multiskin.h"

/****  Globale Variablen ******/

/* Filenamen  */

char bxyfilename[]="Bxy.dat";
char bxfilename[]="Bx.dat";


int output_format=FORMAT_PLAIN;


/***************************************************
 ** Hier wird die Geometrie reingespeichert       **
 ***************************************************/


int  ncoil;          /* Anzahl der Leiter  */
LEITER *leiter=NULL; /* Leitergeometrie aus input file */
MESH *mesh=NULL;     /* 2D Raum */
FILAMENT *filamente; /* Einzelne Strom-Filamente*/
int nmax;            /* Anzahl Filamente */


/* double rho=1/(sigCu);
double Rj=4 *rho * leiterlaenge/(PI * r0*r0);  Widerstand eines Leiters (rund)*/



double mij;
MATRIX Zij,InvZij;
double ztotr,ztoti;
double vtotr,vtoti;


static void fuelle_complex_matrix(MATRIX Zij,int i,int j, double re, double im) {
  Zij.pointer[2*i+2*j*Zij.rows]=re;
  Zij.pointer[2*i+1+(2*j+1)*Zij.rows]=re;
  Zij.pointer[2*i+1+2*j*Zij.rows]=im;
  Zij.pointer[2*i+(2*j+1)*Zij.rows]=-im;
}
static void get_entry_from_complex_matrix(MATRIX Zij,int i,int j,double *re,double *im) {
  *re=Zij.pointer[2*i+1+(2*j+1)*Zij.rows];
  *im=-Zij.pointer[2*i+(2*j+1)*Zij.rows];
}
static void zerlege_complex_matrix(MATRIX Zrij,MATRIX Ziij,MATRIX Zij) {
  int i,j;

  for(i=0;i<Zij.rows/2;i++) {
    for(j=0;j<Zij.rows/2;j++) {
      Zrij.pointer[i+Zij.rows/2*j]=Zij.pointer[2*i+1+(2*j+1)*Zij.rows];
      Ziij.pointer[i+Zij.rows/2*j]=-Zij.pointer[2*i+(2*j+1)*Zij.rows];
    }
  }
}
static MATRIX make_complex_matrix(MATRIX Zrij,MATRIX Ziij) {
  int i,j;
  MATRIX Zij=create_matrix(Zrij.rows*2,Zrij.cols*2);

  for(i=0;i<Zrij.rows;i++) {
    for(j=0;j<Zrij.cols;j++) {
      Zij.pointer[2*i+2*j*(2*Zrij.rows)]=Zrij.pointer[i+Zrij.rows*j];
      Zij.pointer[2*i+1+(2*j+1)*2*Zrij.rows]=Zrij.pointer[i+Zrij.rows*j];
      Zij.pointer[2*i+1+2*j*2*Zrij.rows]=Ziij.pointer[i+Zrij.rows*j];
      Zij.pointer[2*i+(2*j+1)*2*Zrij.rows]=-Ziij.pointer[i+Zrij.rows*j];
    }
  }
  return(Zij);
}


/*******************************************/
/* Berechne Anteil des komplexen Magnetfelds des Filaments n an Ort p */

static void Bpn(VEC3D p, int n, VEC3D *re,VEC3D *im ) {
  VEC3D av,bv,bav,bpv,apv,ev;
  double dd,hpnr,hpni;
	
  av.x=filamente[n].position.x; 	 /* Leiteranfangspunkt */
  av.y=filamente[n].position.y; 
  av.z=leiter[filamente[n].leiter].laenge/2;

  bv.x=av.x;	      /* Leiterendpunkt */
  bv.y=av.y; 
  bv.z=-leiter[filamente[n].leiter].laenge/2;
   
  bav.x=av.x-bv.x; bav.y=av.y-bv.y; bav.z=av.z-bv.z;
  bpv.x= p.x-bv.x; bpv.y= p.y-bv.y; bpv.z= p.z-bv.z;
  apv.x= p.x-av.x; apv.y= p.y-av.y; apv.z= p.z-av.z;

  ev=KPD(bav,bpv);
    
  double d=norm(ev)/norm(bav);     /* Abstand von dem Leiterfilament   */
    
  double cos1=-SPD(bav,apv)/norm(bav)/norm(apv); /* Start und Endwinkel des Leiters*/
  double cos2= SPD(bav,bpv)/norm(bav)/norm(bpv);  

  if(d>r0/2) {  /* Nur Aussenfeld vom Leiter */
		/* Hier Biot-Savat fuer einen Leiter von a nach b  */  
		dd=mu0/(4*PI)*(cos1+cos2)/d/norm(ev);
  } else if(d==0) dd=0; /* Im Zentrum des Leiters ist die Richtung nicht definiert*/
  else {    /* Innenfeld des Leiters  */
		dd=mu0/(4*PI)*(cos1+cos2)/norm(ev)/(r0/2);
  }
  hpnr=filamente[n].ijr*dd;
  hpni=filamente[n].iji*dd;
  re->x=hpnr*ev.x; 
  re->y=hpnr*ev.y; 
  re->z=hpnr*ev.z;
  im->x=hpni*ev.x; 
  im->y=hpni*ev.y; 
  im->z=hpni*ev.z;
}

/* Berechne komplexes Magnetfeld an Ort p */
static void Bp(VEC3D p,VEC3D *re,VEC3D *im) {
  VEC3D zw1,zw2;
  re->x=0;re->y=0;re->z=0;im->x=0;im->y=0;im->z=0;
  for(int i=0;i<nmax;i++) {   /* Bilde complexe Vektorsumme über alle Filamente */
    Bpn(p,i, &zw1, &zw2);
    re->x+=zw1.x; 
    re->y+=zw1.y; 
    re->z+=zw1.z; 
    im->x+=zw2.x; 
    im->y+=zw2.y; 
    im->z+=zw2.z;
  }
}


static void calculate_magnetic_field() {
  int i,j;
  VEC3D v,Br,Bi;
  if(verbose>=0) {printf("...berechne...\r");fflush(stdout);}
  v.z=0.0;    /* Ortsvektor in Z=0 Ebene*/
  for(i=0;i<=meshw;i++) {
    v.x=kx(i);
    for(j=0;j<=meshh;j++) {
      v.y=ky(j);
      Bp(v,&Br,&Bi);
      mesh[i+j*(meshw+1)].Br=Br;
      mesh[i+j*(meshw+1)].Bi=Bi;
    }
  }
}

static void save_magnetic_field(const char *filename) {
  if(verbose>=0) {printf("--> %s [",filename);fflush(stdout);}
  FILE *dptr=fopen(filename,"w");
  fprintf(dptr,"%% MULTISKIN  V. " VERSION "\n"
               "%% Magnetfeldverteilung:\n"
               "%% i j x y Bx.re By.re Bz.re Bx.im By.im Bz.im\n");
  int i,j;
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) {
      fprintf(dptr,"%d %d %g %g %g %g %g %g %g %g\n",i,j,kx(i),ky(j),
      mesh[i+j*(meshw+1)].Br.x,
      mesh[i+j*(meshw+1)].Br.y,
      mesh[i+j*(meshw+1)].Br.z,
      mesh[i+j*(meshw+1)].Bi.x,
      mesh[i+j*(meshw+1)].Bi.y,
      mesh[i+j*(meshw+1)].Bi.z);
    }
  }
  fclose(dptr);
  if(verbose>=0) printf("%d]\n",(meshw+1)*(meshh+1));
}

static void save_magnetic_gradient(const char *filename) {
  if(verbose>=0) {printf("--> %s [",filename);fflush(stdout);}
  FILE *dptr=fopen(filename,"w");
  fprintf(dptr,"%% MULTISKIN  V. " VERSION "\n"
               "%% Magnetfeldverteilung:\n"
               "%% 9. Spalte: ABS(dBy/dx)\n"
               "%% Einheiten: x y in m, Bx.re By.re Bz.re Bx.im By.im Bz.im in T, grad in T/m\n"
               "%% x y Bx.re By.re Bz.re Bx.im By.im Bz.im grad\n");
  double grad=0;
  int i,j=my(0.0);  /* X-Achse, Y=0*/
  VEC3D v,Br,Bi;
  for(i=0;i<=meshw;i++) {
    Br=mesh[i+j*(meshw+1)].Br;
    Bi=mesh[i+j*(meshw+1)].Bi;

    if(i>0) grad=fabs(sqrt(Br.y*Br.y+Bi.y*Bi.y)-v.y)/(r0/2);
    fprintf(dptr,"%g %g %g %g %g %g %g %g %g\n",kx(i),ky(j),
            Br.x,Br.y,Br.z,
	    Bi.x,Bi.y,Bi.z,grad);
      			
    v.x=sqrt(Br.x*Br.x+Bi.x*Bi.x);
    v.y=sqrt(Br.y*Br.y+Bi.y*Bi.y);
    v.z=sqrt(Br.z*Br.z+Bi.z*Bi.z); 
  }
  fclose(dptr);
  if(verbose>=0) printf("%d]\n",meshw+1);
}


/* Berechne Magnetfeld auf den ganzen mesh */

void magnetfeld() {
  if(verbose>=0) printf("\nMagnetfeld:\n===========\n");

  calculate_magnetic_field();

  save_magnetic_field(bxyfilename);


  /* Jetzt noch der Gradient entlang der x-Achse  */
  if(verbose>=0) printf("\ndBy/dx entlang X-Achse:\n=======================\n");

  if(weltymin<=0.0 && weltymax>=0.0) save_magnetic_gradient(bxfilename);
  else printf("ERROR: Mesh-Gebiet enthält die X-Achse nicht.\n");
}

/**************************************************************************/
/* Es werden die Spannungen V0[0...NCOIL-1] berechnet.                    */
/**************************************************************************/

static void impedanz() {
  if(verbose>=0) printf("\nImpedanz-Berechnung:\n====================\n");
  MATRIX Yrmn=create_matrix(ncoil,ncoil);
  MATRIX Yimn=create_matrix(ncoil,ncoil);
  clear_matrix(&Yrmn);
  clear_matrix(&Yimn);
  
  if(verbose>1) printf("-- Gegenimpedanzmatrix fuer die Leiter.\n");
  int     i,j;
  double  re,im;
  int l1,l2;
  for(i=0;i<nmax;i++) {
    for(j=0;j<nmax;j++) {
      get_entry_from_complex_matrix(InvZij,i,j,&re, &im);
      l1=filamente[i].leiter;
      l2=filamente[j].leiter;
      
      Yrmn.pointer[l1+ncoil*l2]+=re; 
      Yimn.pointer[l1+ncoil*l2]+=im;
    }
  }
  
  if(verbose>1) {
    show_matrix(Yrmn);
    show_matrix(Yimn);  
  }

  MATRIX  Ymn=make_complex_matrix(Yrmn,Yimn);
  if(verbose>1) show_matrix(Ymn);
	
  MATRIX Zmn=inverse_matrix(Ymn);
  free_matrix(&Ymn);
  free_matrix(&Yrmn); 
  free_matrix(&Yimn); 
   
  if(verbose>1) {printf("Zmn:");show_matrix(Zmn);}

  MATRIX Zrmn=create_matrix(ncoil,ncoil);
  MATRIX Zimn=create_matrix(ncoil,ncoil);
  zerlege_complex_matrix(Zrmn,Zimn,Zmn);
  free_matrix(&Zmn);
   
  if(verbose>1) {printf("Zrmn:");show_matrix(Zrmn);}
  if(verbose>1) {printf("Zimn:");show_matrix(Zimn);}  
  if(verbose>1) printf("-- Spannungen.\n"); 
  for(i=0;i<ncoil;i++) {
    leiter[i].Vr=0;
    leiter[i].Vi=0;
    for(j=0;j<ncoil;j++) {
      leiter[i].Vr+=Zrmn.pointer[i+j*ncoil]*leiter[j].strom;  /* Hier muesste eigentlich schon */
      leiter[i].Vi+=Zimn.pointer[i+j*ncoil]*leiter[j].strom;  /* eine Phase vorgegeben sein ! */
    }
  }
  vtotr=0; vtotr=0;
  for(i=0;i<ncoil;i++) {
    vtotr+=(leiter[i].strom/abs(leiter[i].strom))*leiter[i].Vr;
    vtoti+=(leiter[i].strom/abs(leiter[i].strom))*leiter[i].Vi;
  }
  if(verbose>1) printf("-- Impedanzen.\n");
  ztotr=0; ztotr=0;
  for(i=0;i<ncoil;i++) {
    for(j=0;j<ncoil;j++) {
      ztotr+=(leiter[i].strom/abs(leiter[i].strom))*(leiter[j].strom/abs(leiter[j].strom))*Zrmn.pointer[i+j*ncoil];
      ztoti+=(leiter[i].strom/abs(leiter[i].strom))*(leiter[j].strom/abs(leiter[j].strom))*Zimn.pointer[i+j*ncoil];
    }
  }
  free_matrix(&Zrmn); 
  free_matrix(&Zimn); 
  if(verbose>-2) {
    if(output_format==FORMAT_TOML) {
      for(i=0;i<ncoil;i++) printf("U_coil[%d] = (%g+i%g)\n",i,leiter[i].Vr,leiter[i].Vi);
      printf("U_total = (%g+i%g)\n",vtotr,vtoti);
      printf("Z_total = (%g+i%g)\n",ztotr,ztoti);
      printf("L = %g\n",-ztoti/w);
      printf("Omega = %g\n",w);
    } else {
      printf("\n===================================================================\n");
      printf("Spannungen ueber den Leitern:\n");
      for(i=0;i<ncoil;i++) {
        printf("Leiter #%d: U=(%g+i%g) V, |U|=%g V, Phase=%g Grad\n",i,
  	  leiter[i].Vr,leiter[i].Vi,sqrt(leiter[i].Vr*leiter[i].Vr+leiter[i].Vi*leiter[i].Vi),
  	  atan2(leiter[i].Vi,leiter[i].Vr)*180/PI);
      }   
      printf("===================================================================\n");
      printf("Gesamtspannungsabfall= (%g +i %g) V  |U|=%g V , Phase=%g Grad\n",
        vtotr,vtoti,sqrt(vtotr*vtotr+vtoti*vtoti),atan2(vtoti,vtotr)*180/PI);
      printf("Gesamtimpedanz:    Z = (%g +i %g) Ohm (R + i Omega L) ]\n",ztotr,ztoti);
      printf("Induktivitaet:     L = %g Henry \n",-ztoti/w);
      printf("mit 	   Omega = %g rad/s \n",w);
      printf("===================================================================\n");
    }
  }
}


static void save_data(char *name,FILAMENT *f,int n) {
  if(verbose>=0) {printf("--> %s [",name);fflush(stdout);}
  FILE *dptr=fopen(name,"w");
  fprintf(dptr,"%% %d Filamente\n" 
               "%% Nr Leiter X Y Ir Ii Iabs\n",n); 
  for(int i=0;i<n;i++) fprintf(dptr,"%d %d %g %g %g %g %g\n",i,f[i].leiter,f[i].position.x,f[i].position.y,
                           f[i].ijr,f[i].iji,f[i].ijabs);
  fclose(dptr);
  if(verbose>=0) printf("%d]\n",n);
}


/******************************************************************/
/* Berechnet die Skineffekte                                      */
/******************************************************************/

void do_calc() {
  if(verbose>=0) {
    printf("\nBERECHNUNG:\n===========\n");
    printf("Frequenz: %g Hz \n",w/2/PI);
    printf("Meshweite: %g mm  \n",r0/mm);
    if(verbose>0) printf(":MESH:[%dx%d]:%ld kBytes.\n",meshw,meshh,(meshw+1)*(meshh+1)*sizeof(MESH)/1000);
  }
  mesh=malloc((meshw+1)*(meshh+1)*sizeof(MESH)); 
  bzero(mesh,(meshw+1)*(meshh+1)*sizeof(MESH));
  int     *meshl=malloc(ncoil*sizeof(int));    /* Anzahl der Mesh-Punkte pro jeweiligem Leiter */
  bzero(meshl,ncoil*sizeof(int));
  
  int i,j;
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) mesh[i+j*(meshw+1)].filament=-1;
  }

  /* nmax rausfinden und tag belegen */ 

  double xpos,ypos;
  int m,n=0;
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) {
      xpos=kx(i);
      ypos=ky(j);
      for(m=0;m<ncoil;m++) { 
	if((leiter[m].xmin<=xpos) && (xpos<=leiter[m].xmax) && (leiter[m].ymin<=ypos) && (ypos<=leiter[m].ymax)) { 
	  mesh[i+j*(meshw+1)].filament=n;
	  meshl[m]++;
	  n++; 
	}
      }
    }
  }
  nmax=n; /* Anzahl der belegten Gitterkaestchen/-punkte = Filamente*/
  if(verbose>=0) {
    printf("Anzahl Filamente: %d\n",nmax);
    printf("Min. Speicherbedarf voraussichtlich: %g MBytes\n",(double)8.0*sizeof(double)*nmax*nmax/1000000.0);
  }

  double re,im,Ri,Li,d,l,zwsp;
  
  if(verbose>0) printf(":FILAMENTE:[%d]:%ld bytes.\n",nmax,nmax*sizeof(FILAMENT));

  filamente=malloc(nmax*sizeof(FILAMENT));
  bzero(filamente,nmax*sizeof(FILAMENT));

  n=0;
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) {
      xpos=kx(i);
      ypos=ky(j);
      for(m=0;m<ncoil;m++) { 
  	if((leiter[m].xmin<=xpos) && (xpos<=leiter[m].xmax) && (leiter[m].ymin<=ypos) && (ypos<=leiter[m].ymax)) {
  	  filamente[n].leiter=m;
  	  filamente[n].position.x=xpos;
  	  filamente[n].position.y=ypos;
  	  n++;
  	}
      }
    }
  }
  
  
  if(!donew && exist(CACHEFILE)) {
    /* Try to load cache */
    InvZij=create_matrix(2*nmax,2*nmax);
    if(verbose>0) printf(":Zinv:%dx%d:%g MBytes.\n",2*nmax,2*nmax,(double)2*nmax*2*nmax*sizeof(double)/1000/1000);
    if(recall_matrix(CACHEFILE,&InvZij)) { /* failed?*/
      donew=1;
      free_matrix(&InvZij);
    }
  }
  
  
  if(donew==TRUE || !exist(CACHEFILE)) {
    if(verbose>0) printf(":Z:%dx%d:%g MBytes.\n",2*nmax,2*nmax,(double)2*nmax*2*nmax*sizeof(double)/1000/1000);
    Zij=create_matrix(2*nmax,2*nmax);  /* Gegenimpedanzen-Matrix  (komplex) */
 
    for(i=0;i<nmax;i++) {
      for(j=i;j<nmax;j++) {
    	if(i==j) { 
    	  /* Ri = Widerstand eines Filaments  */
    	  l=leiter[filamente[i].leiter].laenge;
    	  Ri=leiter[filamente[i].leiter].rho*l/(r0*r0)*(4/PI); /* wir rechnen mit runden 
    	  Filamenten, was nicht ganz korrekt ist, da das mesh ja quadratisch
    	  ist */
    	  /* Li = Selbstinduktivitaet eines Filaments */
    	  zwsp=sqrt(r0*r0/4+l*l);
    	  Li=mu0/8/PI*(l+4*(l*log((l+zwsp)/r0*2)-zwsp+r0/2));
    	  fuelle_complex_matrix(Zij,i,j, Ri, -w*Li);
    	} else {
    	  /* d = Abstand der Filamente   */
    	  d=sqrt((filamente[i].position.x-filamente[j].position.x)*(filamente[i].position.x-filamente[j].position.x)
	         +(filamente[i].position.y-filamente[j].position.y)*(filamente[i].position.y-filamente[j].position.y)
		  ); 
    	  /* Mij = Gegeninduktivitaet */
    	  /* (funktioniert noch nicht fuer verschiedene Filamentlaengen) */
    	  l=leiter[filamente[i].leiter].laenge;
    	  zwsp=sqrt(d*d/l/l+1);
    	  mij=mu0/2/PI*l*(log(l*(zwsp+1)/d)-zwsp+d/l);
    	  fuelle_complex_matrix(Zij,i,j, 0.0, -w*mij);
    	  fuelle_complex_matrix(Zij,j,i, 0.0, -w*mij);
    	}	
      } 
    }
    if(verbose>0) show_matrix(Zij);

    if(verbose>=0) {
      printf("Matrix invertieren:\n");
      if(verbose>0) printf(":Zinv:%dx%d:%g MBytes.\n",2*nmax,2*nmax,(double)2*nmax*2*nmax*sizeof(double)/1000/1000);
      printf("...berechne...\r");fflush(stdout);
    }
    
    InvZij=inverse_matrix(Zij); 
    store_matrix(CACHEFILE,InvZij);
    free_matrix(&Zij); 
  }
 
  if(verbose>0) show_matrix(InvZij);
 
 
  /* Jetzt die Leiterimpedanzen berechnen, daraus den Spannungsabfall ueber den
     Leitern.	*/
 
 
  impedanz();

 
  /* Aus den Spannungen ueber den Leitern jetzt Stromverteilung der Filamente
     berechnen   */


  if(verbose>=0) printf("Stromverteilung berechnen:\n");
	
  for(i=0;i<nmax;i++) {
    filamente[i].ijr=0;
    filamente[i].iji=0;
    for(j=0;j<nmax;j++) {
      /* Achtung: Komplexe mult !!  */
      get_entry_from_complex_matrix(InvZij,i,j,&re, &im);

      filamente[i].ijr+=re*leiter[filamente[j].leiter].Vr-im*leiter[filamente[j].leiter].Vi; 
      filamente[i].iji+=im*leiter[filamente[j].leiter].Vr+re*leiter[filamente[j].leiter].Vi;   
    }
    filamente[i].ijabs=sqrt(filamente[i].ijr*filamente[i].ijr+filamente[i].iji*filamente[i].iji);
  }

  save_data("filamente.dat",filamente,nmax);

  for(i=0;i<ncoil;i++) { leiter[i].itotr=0.0; leiter[i].itoti=0.0; }

  for(i=0;i<nmax;i++) {
    leiter[filamente[i].leiter].itotr+=filamente[i].ijr;
    leiter[filamente[i].leiter].itoti+=filamente[i].iji;
    /* printf("%d %d | %g %g | %g %g \n",i,nc[i],ijr[i],iji[i],itotr[nc[i]],itoti[nc[i]]);*/
  }
  if(verbose>-2) {
    if(output_format==FORMAT_TOML) {
      for(i=0;i<ncoil;i++) printf("I_coil[%d]=(%g+i%g)\n",i,leiter[i].itotr,leiter[i].itoti);
    } else {
      printf("============================================================\n");
      printf("Gesamtstroeme auf den Leitern:\n");
      for(i=0;i<ncoil;i++) {
        printf("Leiter #%d: Mesh=%d  I=(%g+i%g) A, |I|=%g A\n",i,meshl[i],
  	    leiter[i].itotr,leiter[i].itoti,sqrt(leiter[i].itotr*leiter[i].itotr+leiter[i].itoti*leiter[i].itoti));
      }
      printf("============================================================\n");
    }
  }
  if(verbose>=0) {printf("--> mesh.dat [");fflush(stdout);}
  FILE *dptr=fopen("mesh.dat","w");
  fprintf(dptr,"%% Stromdichteverteilung auf Gitter %d x %d\n",meshw,meshh); 
  fprintf(dptr,"%% i j x y Iabs\n"); 
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) fprintf(dptr,"%d %d %g %g %g\n",i,j,kx(i),ky(j),
      (mesh[i+j*(meshw+1)].filament>=0)?filamente[mesh[i+j*(meshw+1)].filament].ijabs:0);
  }
  fclose(dptr);
  if(verbose>=0) printf("%d]\n",(meshw+1)*(meshh+1));
 
 /* Das ganze noch als Matrix ausgeben:  */
  dptr=fopen("wirbel.dat","w");
 
  if(verbose>=0) {printf("--> wirbel.dat [");fflush(stdout);}
  fprintf(dptr,"%% Stromdichteverteilung/Wirbelströme (Absolutbetrag)\n"
               "%% %d x %d\n",meshw,meshh); 
  for(i=0;i<=meshw;i++) {
    for(j=0;j<=meshh;j++) fprintf(dptr,"%g ",(mesh[i+j*(meshw+1)].filament>=0)?filamente[mesh[i+j*(meshw+1)].filament].ijabs:0);  
    fprintf(dptr,"\n");
  }
  fclose(dptr);
  if(verbose>=0) printf("%d]\n",(meshw+1)*(meshh+1));

  free_matrix(&InvZij);free(meshl);
}

void multiskin_free() {
  /* free memory */
  free(filamente);
  free(mesh);
  free(leiter);
}
