/* matrix.c (c) Markus Hoffmann  */

/* This file is part of MULTISKIN, eddie current calculation
 * ==============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
 
#include "vector.h"
#include "matrix.h"
#include "multiskin.h"

extern int verbose;


MATRIX create_matrix(int rows,int cols) {
  MATRIX ergeb;
  ergeb.rows=rows;
  ergeb.cols=cols;
  ergeb.pointer=malloc(rows*cols*sizeof(double));
  return(ergeb);
}

void clear_matrix(MATRIX *a) {
  int i;
  for(i=0;i<(a->rows)*(a->cols);i++) (a->pointer)[i]=0;
}
void free_matrix(MATRIX *a) {
  free(a->pointer);
}

MATRIX clone_matrix(MATRIX a) {
  MATRIX ergeb;
  ergeb.rows=a.rows;
  ergeb.cols=a.cols;
  ergeb.pointer=malloc(a.rows*a.cols*sizeof(double));
  memmove(ergeb.pointer,a.pointer,a.rows*a.cols*sizeof(double));
  return(ergeb);
}

void show_matrix(MATRIX a) {
  int i,j,rows,cols;

  printf("\nMatrix: %dx%d\n",a.rows,a.cols);
  printf("================\n\n");
  rows=min(a.rows,8);
  cols=min(a.cols,8);
  for(i=0;i<rows;i++) {
    for(j=0;j<cols;j++) printf("%6.3g ",a.pointer[i+a.rows*j]);	  
    printf("\n");
  }
}

void store_matrix(const char *filename, const MATRIX a) {
  if(verbose>=0) printf("--> %s %dx%d\n",CACHEFILE,a.rows,a.cols);
  int fdis=open(filename,O_CREAT|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP);
  if(fdis==-1) {perror("open");return;}
  if(write(fdis,&a,sizeof(MATRIX))==-1) perror("write");
  if(write(fdis,a.pointer,a.rows*a.cols*sizeof(double))==-1) perror("write");
  close(fdis);
}
int recall_matrix(const char *filename, MATRIX *a) {
  MATRIX b;
  int ret=0;
  if(verbose>=0) printf("<-- %s %dx%d\n",CACHEFILE,a->rows,a->cols);
  int fdis=open(filename,O_RDONLY);
  if(fdis==-1) {perror("open");return(-1);}
  if(read(fdis,&b,sizeof(MATRIX))<0) {perror("read");ret=-1;}
  else {
    if(a->rows!=b.rows || a->cols!=b.cols) { 
      printf("Error: cache does not contain correct matrix.\n");
      ret=-1;
    } else {
      if(read(fdis,a->pointer,a->rows*a->cols*sizeof(double))<0) {perror("read");ret=-1;}
    }
  }
  close(fdis);
  return(ret);
}



/* Solves the equation system Ax=b with singular value decomposition */

#ifdef ANDROID
 int dgelss_(long int *m, long int *n, long int *nrhs, double *a, 
             long int *lda, double *b, long int *ldb, double *s, double *rcond, 
	     long int *rank, double *work, long int *lwork, long int *info);
#else
 int dgelss_(int *m, int *n, int *nrhs, double *a, int *lda, double *b, 
             int *ldb, double *s, double *rcond, int *rank, double *work, 
	     int *lwork, int *info);
#endif
 
MATRIX solve(MATRIX a,MATRIX b) {
  /* Hier probieren wir noch einen Algoritmus mit SVD    */
#ifdef ANDROID
  long int lda=a.rows;
  long int ldb=max(a.rows,a.cols);
  long int nrhs=1;
  long int rank;
  long int info;
  long int lwork=3*min(a.cols,a.rows) + 
       max(2*min(a.cols,a.rows), max(a.cols,a.rows))+nrhs+10*a.rows;
#else
  int lda=a.rows,ldb=max(a.rows,a.cols),nrhs=1,rank,info;
  int lwork=3*min(a.cols,a.rows) + 
       max(2*min(a.cols,a.rows), max(a.cols,a.rows))+nrhs+10*a.rows;
#endif
  double rcond=1e-10;
  double work[lwork];
  b.pointer=realloc(b.pointer,sizeof(double)*ldb);
  double singulars[a.cols];
  
  dgelss_(&a.rows,&a.cols,&nrhs,a.pointer,&lda,b.pointer,&ldb,singulars,
    &rcond,&rank,work,&lwork,&info);
  if(info) {
    printf("ERROR: the solver has not been successful.\n");
#ifdef ANDROID
    if(info<0) printf("the %ld-th argument had an illegal value.\n",-info);
    else printf("the algorithm for computing the SVD failed to converge;\n"  
                "%ld off-diagonal elements of an intermediate "
                " bidiagonal form did not converge to zero.\n",info);
#else
    if(info<0) printf("the %d-th argument had an illegal value.\n",-info);
    else printf("the algorithm for computing the SVD failed to converge;\n"  
                "%d off-diagonal elements of an intermediate "
                " bidiagonal form did not converge to zero.\n",info);
#endif
    exit(-1);
  }

  if(verbose>0) printf("INFO: accuracy: %g%%\n",(double)rank/a.rows*100);
#ifdef ANDROID
  if(verbose>1) printf("lwork=%ld optimal: %g\n",lwork,work[0]);
#else
  if(verbose>1) printf("lwork=%d optimal: %g\n",lwork,work[0]);
#endif

  if(verbose>1) {
    int i;
    printf("First 20 singular values:\n");
    for(i=0;i<min(20,a.cols);i++) printf("%d: %g\n",i,singulars[i]);
  }
  
  if((double)rank<(double)a.cols/6) printf("WARNING: The solution will not be very accurate!\n");
  return(b);
}

// LU decomoposition of a general matrix
void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

// generate inverse of a matrix given its LU decomposition
void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);


MATRIX inverse_matrix(MATRIX b) {
    MATRIX a=clone_matrix(b);
    int lda=max(1,a.rows);
    int *ipiv=malloc(sizeof(int)*min(a.rows,a.cols));
    int lwork=lda*4;
    double *work=malloc(sizeof(double)*lwork);
    int info;

    dgetrf_(&a.rows,&a.cols,a.pointer,&lda,ipiv,&info);
    if(info<0) printf("the %d-th argument had an illegal value\n",-info);
    else if(info>0) printf("U(%d,%d) is exactly zero.\n",info,info);
    
    dgetri_(&a.cols,a.pointer,&lda,ipiv,work,&lwork,&info);
    free(ipiv);
    return(a);
}
