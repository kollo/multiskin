/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */

extern double weltxmin,weltxmax,weltymin,weltymax;
extern int meshw, meshh;

/* Function prototypes */

int mx(double x);
int my(double y);
double kx(int x);
double ky(int y);
