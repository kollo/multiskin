#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



#include "vector.h"


/* Kreuzprodukt */
VEC3D KPD(VEC3D a, VEC3D b) {
  VEC3D ret;
  ret.x=a.y*b.z-b.y*a.z;
  ret.y=a.z*b.x-b.z*a.x;
  ret.z=a.x*b.y-b.x*a.y;
  return(ret);
}

double norm(VEC3D a) 	{return(sqrt(a.x*a.x+a.y*a.y+a.z*a.z));}
/* Skalarprodukt */
double SPD(VEC3D a, VEC3D b) 	{return(a.x*b.x+a.y*b.y+a.z*b.z);}
