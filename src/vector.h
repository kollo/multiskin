
/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */



typedef struct {double x; double y; double z;} VEC3D;
typedef struct {double x; double y; } VEC2D;




VEC3D KPD(VEC3D a, VEC3D b);
double norm(VEC3D a);
double SPD(VEC3D a, VEC3D b);
