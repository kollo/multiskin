/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "file.h"
#include "tools.h"

#include "vector.h"
#include "multiskin.h"
#include "geometry.h"

double weltxmin,weltxmax,weltymin,weltymax;
int meshw, meshh;

static char *next_wort(char *p) {
  char *r=p;
  while(*r!=' ' && *r!=0) r++;
  *r=0;
  return(++r);
}


/* Geometriedaten aus File einlesen */

void read_file(char *name) {
  char w1[MAXSTRLEN],w2[MAXSTRLEN],zeile[MAXSTRLEN];
  char *p,*q;

  if(verbose>=0) printf("\nGEOMETRIEDATEN EINLESEN:\n========================\n"); 

  ncoil=0;
  if(leiter) {free(leiter);leiter=NULL;}

  if(verbose>=0) {printf("<-- %s ",name);fflush(stdout);}
  FILE *dptr=fopen(name,"r");
 
  /* Zunaechst anzahl der Leiter und den maximal zu berechnenden Bereich rausfinden */
 
  while(feof(dptr)==0) {
    lineinput(dptr,w1,MAXSTRLEN);
    xtrim(w1,TRUE,zeile);
    if(verbose>2) printf("> %s\n",zeile);
    wort_sep(zeile,'!',TRUE,zeile,w1); 
    wort_sep(zeile,' ',TRUE,w1,w2);
    if(!strcmp(w1,"REM"))          ;
    else if(!strcmp(w1,"'"))       ;
    else if(*w1==0)           ;  /* nixtun, leerzeile */
    else if(*w1=='#')           ;  /* nixtun, Kommentarzeile */
    else if(!strcmp(w1,"LEITER")) ncoil++;
    else if(!strcmp(w1,"WELT")) ;
    else ncoil++;
  }
  fclose(dptr);

  if(verbose>0) {printf("(%d Leiter) [",ncoil);fflush(stdout);}
  leiter=malloc(ncoil*sizeof(LEITER));
  
  weltxmin=r0*990/mm; weltxmax=-r0*990/mm;
  weltymin=r0*990/mm; weltymax=-r0*990/mm;
  
  
  ncoil=0;
  dptr=fopen(name,"r");
  while(feof(dptr)==0) {
    lineinput(dptr,w1,MAXSTRLEN);
    xtrim(w1,TRUE,zeile); 
    if(*zeile!='#' && *zeile!='\'' && *zeile!=0 && *zeile!='%') {
      p=next_wort(zeile);
      leiter[ncoil].xmin=atof(zeile);
      weltxmin=min(leiter[ncoil].xmin,weltxmin);
      q=next_wort(p);
      leiter[ncoil].xmax=atof(p); 
      weltxmax=max(leiter[ncoil].xmax,weltxmax);
      p=next_wort(q);
      leiter[ncoil].ymin=atof(q);  
      weltymin=min(leiter[ncoil].ymin,weltymin);
      q=next_wort(p);
      leiter[ncoil].ymax=atof(p);
      weltymax=max(leiter[ncoil].ymax,weltymax);
      p=next_wort(q); 
      leiter[ncoil].laenge=mm*atof(q);
      q=next_wort(p);
      leiter[ncoil].strom=atof(p); 
      p=next_wort(q);
      leiter[ncoil].rho=1e-8*atof(q);
        
      if(verbose>1) printf("Leiter #%d: (%g:%g)(%g:%g) l=%g rho=%g I=%g\n",ncoil,
          leiter[ncoil].xmin,leiter[ncoil].xmax,
          leiter[ncoil].ymin,leiter[ncoil].ymax,
          leiter[ncoil].laenge,leiter[ncoil].rho,leiter[ncoil].strom);
      else if(verbose>0) {printf(".");fflush(stdout);}  

      if(leiter[ncoil].strom!=0.0) ncoil++;
    }
  }
  fclose(dptr);
  if(verbose>0) printf("] %d\n",ncoil);
  else if(verbose>=0) printf("%d\n",ncoil);
  
  meshw=(weltxmax-weltxmin)*mm/r0;
  meshh=(weltymax-weltymin)*mm/r0;
  
  if(verbose>=0) {
    printf("%d Leiter gefunden.\n",ncoil);
    printf("X=[%g:%g] mm\n",weltxmin,weltxmax);
    printf("Y=[%g:%g] mm\n",weltymin,weltymax);
    printf("mm=%g r0=%g\n",mm,r0);
    printf("Mesh: %d x %d\n",meshw,meshh);
  }
}

int mx(double x) {return((int)(x-weltxmin)/(weltxmax-weltxmin)*meshw);}
int my(double y) {return((int)(y-weltymin)/(weltymax-weltymin)*meshh);}

double kx(int x) {return((double)x/meshw*(weltxmax-weltxmin)+weltxmin);}
double ky(int y) {return((double)y/meshh*(weltymax-weltymin)+weltymin);}
