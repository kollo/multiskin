/* Erweiterungen fuer die Datei Ein- und Ausgabe ....   */
/* von Markus Hoffmann                                  */
/*                                                      */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h> 

#define FALSE 0
#define TRUE (!FALSE)

#include "file.h"


char *lineinput( FILE *n, char *line,int size) {   /* liest eine ganze Zeile aus einem ASCII-File ein */
  int c; int i=0;
  while((c=fgetc(n))!=EOF) {
      if(c==(int)'\n') {
	  line[i]=(int)'\0';
	  return line;
	}
	else line[i++]=(char)c;
	if(i>=size-1) break;
    }
    line[i]='\0';
    return line;
}


/* Return 0 if file does not exist. */

int exist(const char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(0);
  return(!0);
}
