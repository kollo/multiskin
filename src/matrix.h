
/* This file is part of MULTISKIN, eddie current calculation
 * ============================================================
 * MULTISKIN is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */




typedef struct {
#ifdef ANDROID
  long int rows;
  long int cols;
#else
  int rows;
  int cols;
#endif
  double *pointer;
} MATRIX;



#define max(a,b) ((a>b)?a:b)
#define min(a,b) ((a<b)?a:b)




#define MAT(m,a,b) ((m.pointer)[a+(b)*m.rows])
#ifdef __cplusplus
extern "C" {
#endif
MATRIX create_matrix(int rows,int cols);
void clear_matrix(MATRIX *a);
void free_matrix(MATRIX *a);
MATRIX solve(MATRIX,MATRIX);
MATRIX SVD(MATRIX a, MATRIX *w, MATRIX *v);
MATRIX backsub(MATRIX singulars, MATRIX u, MATRIX v, MATRIX b);
MATRIX inverse_matrix(MATRIX A);
MATRIX clone_matrix(MATRIX a);
void show_matrix(MATRIX a);
void store_matrix(const char *filename, const MATRIX a);
int recall_matrix(const char *filename, MATRIX *a);

#ifdef __cplusplus
}
#endif
