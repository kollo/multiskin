<img alt="MULTISKIN" src="artwork/multiskin-logo.png"/>

MULTISKIN
=========
(c) by Markus Hoffmann 1997-2019

Copyright (c) 1997-1998 -- Physikalisches Institut der Universitaet Bonn

an eddi current calculator for 2D conductor geometries.

Description
-----------


Multiskin calculates skin effect and eddy currents in a 2D conductor geometry.  
Also the frequency dependant 
impedance (inductivity and resitance) can be calculated.
multiskin is used to calculate the properties of pulsed magnets.

The geometry is defined in ASCII files, which
are taken by multiskin as input.  With commandline parameters you can specify the frequency of the  currents  for  which  the
magnetic  field  and current distribution will be calculated.  Multiskin calculates the impedances, current distribution, and
the magnetic field distribution.

multiskin compiles on linux. It can also produce an excecutable for
WINDOWS (multiskin.exe) which can be run from the command shell.

Usually the outputs and files produced by multiskin can be processed
together with gnuplot, which should be installed.

This program is scientific software. The used methods and formulas are
published in: Markus Hoffmann, 'Bau und Test gepulster Quadrupolmagnete 
zur Querung intrinsischer depolarisierender Resonanzen in ELSA', 
BONN-IB-98-10, Bonn University, March 1998.

This program is open source and licensed under the GPLv2.
This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


german
------
Multiskin berechnet die Stromverteilung, die Impedanz und Magnetfeld
in einer (2-dimensionalen) Leitergeometrie. 

Hiermit können näherungsweise (lange) gepulste Magnete mit dicken 
Leitern berechnet werden. 

Auf diese weise wird der Skin-Effekt durch Wirbelströme berechnet, dieser
führt zu einer ungleichmäßigen Stromverteilung innerhalb der Leiter und
beeinflusst somit Impedanz, Magnetfeldverteilung und Stromdichte.


Das Programm multiskin wurde an der Uni-Bonn 1998 erstellt. 
Die verwendeten Methoden und Formeln sind im Detail hier erläutert:

Markus Hoffmann, 'Bau und Test gepulster Quadrupolmagnete 
zur Querung intrinsischer depolarisierender Resonanzen in ELSA', 
BONN-IB-98-10, Bonn University, March 1998.

### Important Note:

    MULTISKIN is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
    (Basically that means, free, open source, use and modify as you like, don't
    incorporate it into non-free software, no warranty of any sort, don't blame me
    if it doesn't work.)
    
    Please read the file INSTALL for compiling instructions.

