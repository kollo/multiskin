Guide to contributing to MULTISKIN
==================================

MULTISKIN is a commandline program for Linux and WINDOWS. It has 
been along since 1997 and has been used to design several pulsed magnets for 
accelerators, not only at ELSA.

The functionallyty based on this principle is limited, but the program and also
the whole package can of course be improved.

However, changes in the base functionallity are planned to always be backported
into this repository, and if you like, we will try to keep it running on the
WINDOWS platform (however, this has always been tricky) and on debian linux.

WINDOWS support is depreciated. But if you are a WINDOWS expert and want to take
over... please do so.

Suggestions for improvements are therefor very welcome. Even the exchange 
of experience of usage could be very valuable.

The new version of MULTISKIN is still file compatible (.inp and .geo. 
files, as well as the resulting .dat files with the calculated data). 
I would like to keep this as a feature even though the syntax could be improved
and cleaned up a bit.


You can:
* report bugs and unexpected behaviour. --> open issue
* donate/share your magnet or geometry files for testing.
* discuss new features  --> open issue
* improve the user manual and online-help.


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Merge Request

The best way to contribute to this project is by making a merge request:

1. Login with your codeberg account or create one now
2. [Fork](https://codeberg.org/kollo/multiskin.git) the multiskin repository.  Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the  `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or codeberg ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge request against the multiskin repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/multiskin/issues/new?issue) now!


## Thanks

We are very grateful for your support. With your help, this implementation
will be a great project. 
