cd data
mkdir -p f1000m
mkdir -p f2000m
mkdir -p f5000m
mkdir -p f10000m
mkdir -p f20000m
mkdir -p f50000m
mkdir -p f100000m
cd f1000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 1000  --mag > blech 
cd ..
cd f2000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 2000  --mag > blech 
cd ..
cd f5000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 5000  --mag > blech 
cd ..
cd f10000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 10000 --mag > blech 
cd ..
cd f20000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 20000 --mag > blech 
cd ..
cd f50000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 50000 --mag > blech 
cd ..
cd f100000m
rm -f blech
multiskin  ../tunejump_m.inp --mesh 1.5 --freq 100000 --mag > blech 
cd ..
