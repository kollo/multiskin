mkdir -p data/f1o
cd data/f1o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 1  --mag > blech 
cd ..
mkdir -p f10o
cd f10o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 10 --mag > blech 
cd ..
mkdir -p f50o
cd f50o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 50 --mag > blech 
cd ..
mkdir -p f100o
cd f100o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 100 --mag > blech 
cd ..
mkdir -p f200o
cd f200o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 200 --mag > blech 
cd ..
mkdir -p f300o
cd f300o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 300 --mag > blech 
cd ..
mkdir -p f500o
cd f500o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 500  --mag > blech 
cd ..
mkdir -p f1000o
cd f1000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 1000 --mag > blech 
cd ..
mkdir -p f5000o
cd f5000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 5000 --mag > blech 
cd ..
mkdir -p f10000o
cd f10000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 10000 --mag > blech 
cd ..
mkdir -p f20000o
cd f20000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 20000 --mag > blech 
cd ..
mkdir -p f50000o
cd f50000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 50000 --mag > blech 
cd ..
mkdir -p f100000o
cd f100000o
rm -f blech
multiskin ../../geometries/tunejump_o.inp  --mesh 1 --freq 100000 --mag > blech 
