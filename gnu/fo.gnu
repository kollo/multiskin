set yrange [0:1.0]
set xrange [-30:30]
set data style lines
set title "Feldgradient fuer verschiedenen Frequenzen"
set xlabel "X"
set ylabel "dBy/dx"
plot "f1o/Bx.dat" using 1:9,"f10o/Bx.dat" using 1:9,"f50o/Bx.dat" using 1:9,\
     "f100o/Bx.dat" using 1:9,"f200o/Bx.dat" using 1:9,\
     "f300o/Bx.dat" using 1:9, "f500o/Bx.dat" using 1:9,\
     "f1000o/Bx.dat" using 1:9,"f5000o/Bx.dat" using 1:9, \
     "f10000o/Bx.dat" using 1:9, "f20000o/Bx.dat" using 1:9 ,\
     "f50000o/Bx.dat" using 1:9, "f100000o/Bx.dat" using 1:9 
pause -1


set term post col sol
set output "grad.ps"

plot "f1o/Bx.dat" using 1:9,"f10o/Bx.dat" using 1:9,"f50o/Bx.dat" using 1:9,\
     "f100o/Bx.dat" using 1:9,"f200o/Bx.dat" using 1:9, \
     "f300o/Bx.dat" using 1:9,"f500o/Bx.dat" using 1:9, \
     "f1000o/Bx.dat" using 1:9,"f5000o/Bx.dat" using 1:9, \
     "f10000o/Bx.dat" using 1:9, "f20000o/Bx.dat" using 1:9 ,\
     "f50000o/Bx.dat" using 1:9, "f100000o/Bx.dat" using 1:9 
