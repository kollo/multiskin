set yrange [0:1.4]
set xrange [-0.04:0.04]
set data style lines
#set title "Field gradient inside the tunejump magnet"
set xlabel "X [m]"
set ylabel "dBy/dx [T/m]"
plot "f1m/Bx.dat" using 1:9 t "1 Hz",\
     "f5m/Bx.dat" using 1:9 t "5 Hz",\
     "f10m/Bx.dat" using 1:9 t "10 Hz",\
     "f20m/Bx.dat" using 1:9 t "20 Hz",\
     "f50m/Bx.dat" using 1:9 t "50 Hz",\
     "f100m/Bx.dat" using 1:9 t "100 Hz",\
     "f200m/Bx.dat" using 1:9 t "200 Hz",\
     "f500m/Bx.dat" using 1:9 t "500 Hz", \
     "f1000m/Bx.dat" using 1:9 t "1 kHz",\
     "f2000m/Bx.dat" using 1:9 t "2 kHz",\
     "f5000m/Bx.dat" using 1:9 t "5 kHz", \
     "f10000m/Bx.dat" using 1:9 t "10 kHz",\
      "f20000m/Bx.dat" using 1:9 t "20 kHz",\
     "f50000m/Bx.dat" using 1:9 t "50 kHz",\
      "f100000m/Bx.dat" using 1:9 t "100 kHz"
pause -1
set term post col sol
set output "gradm.ps"

plot "f1m/Bx.dat" using 1:9 t "1 Hz",\
     "f5m/Bx.dat" using 1:9 t "5 Hz",\
     "f10m/Bx.dat" using 1:9 t "10 Hz",\
     "f20m/Bx.dat" using 1:9 t "20 Hz",\
     "f50m/Bx.dat" using 1:9 t "50 Hz",\
     "f100m/Bx.dat" using 1:9 t "100 Hz",\
     "f200m/Bx.dat" using 1:9 t "200 Hz",\
     "f500m/Bx.dat" using 1:9 t "500 Hz", \
     "f1000m/Bx.dat" using 1:9 t "1 kHz",\
     "f2000m/Bx.dat" using 1:9 t "2 kHz",\
     "f5000m/Bx.dat" using 1:9 t "5 kHz", \
     "f10000m/Bx.dat" using 1:9 t "10 kHz",\
      "f20000m/Bx.dat" using 1:9 t "20 kHz",\
     "f50000m/Bx.dat" using 1:9 t "50 kHz",\
      "f100000m/Bx.dat" using 1:9 t "100 kHz"


