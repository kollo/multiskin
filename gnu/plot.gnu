#* This file is part of MULTISKIN, eddie current calculation
#* ============================================================
#* MULTISKIN is scientific software and comes with NO WARRANTY - read the file
#* COPYING for details.
#*
set term x11
set grid
set title "Magnetfeld auf X-Achse"
set xlabel "Y-pos [mm]"
plot "Bx.dat" u 1:3 w st t "B_x_r", "" u 1:4 w st t "B_y_r", "" u 1:5 w st t "B_z_r", \
"" u 1:6 w st t "B_x_i", "" u 1:7 w st t "B_y_i", "" u 1:8 w st t "B_z_i"
pause -1

set title "Gradient entlang X-Achse"
plot "Bx.dat" u 1:9 w st t "Gradient dBy/dy"
pause -1

set parametric
set xrange [:]
set yrange [:]
set xlabel "X [mm]"
set ylabel "Y [mm]"
set zlabel "Strom"
set title "Stromdichteverteilung"

splot "mesh.dat" using 3:4:5 w lines t ""
pause -1

set term post eps col sol 22

set output "mesh-stromdichte.eps"
replot
