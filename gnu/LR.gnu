
set title "Frequenzabhaengigkeit der Impedanz des Magneten"
set xlabel "Hz"
set ylabel "mOhm"
set grid
set yrange [0.5:170]

set logscale
plot "LRm.dat" using 1:3 w linespoints, \
"LRmessungklein.dat" using 1:3:6 w lines,\
"LRmessungklein.dat" using 1:3:6 t "" w errorbars 
pause -1
set ylabel "muH"
set yrange [1.9:6]
plot "LRm.dat" using 1:2 w linespoints, \
"LRmessungklein.dat" using 1:2:5 w lines,\
"LRmessungklein.dat" using 1:2:5 t "" w errorbars 

pause -1
set ylabel "T/m"
set yrange [0.1:1.2]
plot "LRo.dat" using 1:4 w linespoints

pause -1

set term post col sol
set output "R.ps"
set grid
set yrange [0.5:170]
set ylabel "mOhm"
set logscale
plot "LRm.dat" using 1:3 w linespoints, \
"LRmessungklein.dat" using 1:3:6 w lines,\
"LRmessungklein.dat" using 1:3:6 t "" w errorbars 
set output "L.ps"
set ylabel "muH"
set yrange [1.9:6]
plot "LRm.dat" using 1:2 w linespoints, \
"LRmessungklein.dat" using 1:2:5 w lines,\
"LRmessungklein.dat" using 1:2:5 t "" w errorbars 
